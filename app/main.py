from fastapi import FastAPI

from app.user import user_router

app = FastAPI()
app.include_router(user_router)


@app.get("/")
async def index():
    return {"title": "Hello World :)"}
