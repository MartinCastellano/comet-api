from app.user.model.model import UserCredentials
import json
from typing import Dict
import logging



class UserService():

    def __init__(self):
        log = logging.basicConfig(
            filename='profile_data.log',
            format="%(message)s" 
        )
    
        
    def get_variables(self, credentials: UserCredentials) -> Dict[str, str]:

        variables = {
            "email": credentials.email,
            "password": credentials.password
        }
        return variables

    def get_payload(self, variables: dict) -> str:
        payload_str = {
            "query": "mutation authenticate($email: EmailType!, $password: String!) { authenticate(email: $email, password: $password) { ...User } } fragment User on User { email firstName fullName jobTitle lastName phoneNumber profilePictureUrl freelance { ...LinkedInImport } } fragment LinkedInImport on Freelance { biography experienceInYears experiences { companyName description location skills { name } } }",
            "operationName": "authenticate",
            "variables": variables
        }
        payload_json = json.dumps(payload_str)
        return payload_json

    def get_headers(self) -> Dict[str, str]:

        headers = {
            'Content-Type': "application/json",
            'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'
        }

        return headers
      
       
    def get_data(self,datjs:json):            
        data =datjs                        
        if "data" not in data or data["data"]["authenticate"] is None:
            error_message = str(data["errors"][0]["message"])
            raise Exception(error_message)
        else:
            data["data"]["authenticate"]
            return data["data"]["authenticate"]
            
            