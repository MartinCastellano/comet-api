
from http import HTTPStatus


class NotFoundException(BaseException):
    def __init__(self, message):
        super().__init__(self)
        self.message = message
        self.status = HTTPStatus.NOT_FOUND


class ValidationErrorException(BaseException):
    def __init__(self, message):
        super().__init__(self)
        self.message = message
        self.status = HTTPStatus.BAD_REQUEST
