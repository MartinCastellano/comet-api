
from app.user.model.model import UserCredentials
import app.user.service as service
from app.user.exceptions import exceptions
from fastapi import HTTPException
import httpx
import logging


async def fetch_user_data(credentials: UserCredentials):

    user_service = service.UserService()
    variables = user_service.get_variables(credentials)
    payload = user_service.get_payload(variables)
    headers = user_service.get_headers()

    try:
        async with httpx.AsyncClient() as client:    
            res = await client.request(method='POST', url='https://api.comet.co/api/graphql',headers=headers,data=payload)
            logging.log(level=res.status_code,msg= res.json())           
            return user_service.get_data(res.json())
    
    except httpx.RequestError as exc:
        print(f"An error occurred while requesting {exc.request.url!r}.") 
    
    except exceptions.NotFoundException as e:
        raise HTTPException(
            status_code=e.status_code,
            detail=e.message,
        )
    except exceptions.ValidationErrorException as e:
        raise HTTPException(
            status_code=e.status_code,
            detail=e.message,
        )
    
