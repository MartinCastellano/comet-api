from fastapi import APIRouter
from .model.model import UserCredentials
from .controller.controller import fetch_user_data

router = APIRouter()

@router.post("/fetch")
async def fetch_in_user(credentials: UserCredentials):    
    profile_data = await fetch_user_data(credentials)
    return profile_data